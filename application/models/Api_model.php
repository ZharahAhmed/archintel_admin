<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_model extends CI_Model {

	function ValidAPIKey($key){
      $this->db->select('ID');
      $this->db->from('api_keys');
      $this->db->where('api_key', $key);
      $this->db->where('status', 'Active');

      return $this->db->get()->num_rows() === 1 ? true : false;
   }
}
