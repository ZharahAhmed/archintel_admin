<?php
class Account_model extends CI_Model {

    public function __construct()
    {
            $this->load->database();
    }


	public function view($id){

        $query = $this->db->get_where('users', array('id'=>$id));
        return $query->row_array();

    }

     function update($id,$data){
         $this->db->select('ID');

        $this->db->from('users');
        $this->db->where('ID', $id);
        $this->db->update('users', $data);

        $this->load->view('tpl/full-page/header');
        $this->load->view('tpl/full-page/navbar');
        $this->load->view('Account/done');
        $this->load->view('tpl/full-page/footer');

   }


     function delete($id,$data){
         $this->db->select('ID');

        $this->db->where('ID', $id);
        $this->db->delete('users', $data);

        $this->load->view('tpl/full-page/header');
        $this->load->view('tpl/full-page/navbar');
        $this->load->view('Account/done');
        $this->load->view('tpl/full-page/footer');

   }

  public function adduser()
    {

    	 $this->load->library('bcrypt');

            $data_ary = array(
           
            'username' => $this->input->post('username'),
            'email' => $this->input->post('email'),
           'password' => $this->bcrypt->hash($this->input->post('password')),

            );           
        return $this->db->insert('users', $data_ary);  

           $this->load->view('tpl/full-page/header');
        $this->load->view('tpl/full-page/navbar');
        $this->load->view('Account/done');
        $this->load->view('tpl/full-page/footer'); 
    }
}