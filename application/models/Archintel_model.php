<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Archintel_model extends CI_Model {

    public function __construct()
    {
            $this->load->database();
    }
    public function getalluser()
    {
        $query = $this->db->get('users');
        return $query->result_array();
    }

   function AddCompany($data){
      $this->db->insert('companies', $data);

      return $this->db->affected_rows() === 1 ? true : false;
   }

   function AllCompanyHistoricalGraphData($start, $end){
      $this->db->select('ID');
      $this->db->select('company_id');
      $this->db->select('date');
      $this->db->select('high');
      $this->db->select('open');
      $this->db->select('close');
      $this->db->select('low');
      $this->db->select('adj_close');
      $this->db->select('volume');
      $this->db->from('historical_data');
      $this->db->where('date >=', $start);
      $this->db->where('date <=', $end);

      return $this->db->get();
   }

   function CompanyDetails($value, $field = 'ID'){
      $this->db->select('ID');
      $this->db->select('symbol');
      $this->db->select('name');
      $this->db->select('sp');
      $this->db->select('status');
      $this->db->from('companies');
      $this->db->where($field, $value);

      return $this->db->get()->row();
   }

   function DeleteCompany($id){
      $this->db->where('ID', $id);
      $this->db->delete('companies');

      return $this->db->affected_rows() == 1 ? true : false;
   }

   function GetCompanies($active_only = false){
      $this->db->select('ID');
      $this->db->select('symbol');
      $this->db->select('name');
      $this->db->select('sp');
      $this->db->select('status');
      $this->db->from('companies');
      $this->db->order_by('name','ASC');

      if($active_only)
         $this->db->where('status', 'Active');

      return $this->db->get();
   }

   function GetDefinedName($symbol){
      $this->db->select('name');
      $this->db->from('companies');
      $this->db->where('symbol', $symbol);

      return $this->db->get()->row()->name;
   }

   function LastMediaDate(){
      $this->db->select('Date__c');
      $this->db->from('media');
      $this->db->order_by('Date__c','DESC');
      $this->db->limit(1);

      $res = $this->db->get();

      return $res->num_rows() == 0 ? '01/01/1970' : $res->row()->Date__c;
   }

   function LocalHistoricalData($symbol, $start, $end){
      $this->db->select('h.ID as hID');
      $this->db->select('company_id');
      $this->db->select('date');
      $this->db->select('high');
      $this->db->select('open');
      $this->db->select('close');
      $this->db->select('low');
      $this->db->select('adj_close');
      $this->db->select('volume');
      $this->db->from('historical_data h');
      $this->db->join('companies c', 'c.ID = h.company_id');
      $this->db->where_in('symbol', $symbol);
      $this->db->where('date >=', $start);
      $this->db->where('date <=', $end);

      return $this->db->get();
   }

   function UpdateAccount($data, $id){
      $this->db->where('ArchIntelId__c', $id);
      $this->db->update('accounts', $data);

      return $this->db->affected_rows() > 0 ? true : false;
   }

   function UpdateCompany($data, $id){
      $this->db->where('ID', $id);
      $this->db->update('companies', $data);

      return true;
   }

   function UploadAccountFromSalesforce($account, $contacts){
      $this->db->trans_start();
      $this->db->insert_batch('accounts', $account);
      $this->db->insert_batch('contacts', $contacts);
      $this->db->trans_complete();

      return $this->db->trans_status() === true ? true : false;
   }

   function UploadHistoricalData($data){
      $this->db->insert_batch('historical_data', $data);

      return $this->db->affected_rows() > 0 ? true : false;
   }

   function UploadMedia($data){
      if(!empty($data)){
         $this->db->insert_batch('media', $data);
         return $this->db->affected_rows() > 0 ? true : false;
      }
      return true;
   }


}
