<section class="row">
   <div class="col-md-4 col-md-offset-4">
      <h4 class="heading text-center">Update Company</h4>
      <div class="col-md-12 bordered bordered-no-top bottom-offset-20">
         <?php echo form_open('',array('class' => 'form top-offset-20')); ?>
            <div class="form-group col-md-12">
               <label for="symbol">Symbol</label>
               <input type="hidden" name="id" value="<?php echo $details->ID; ?>">
               <input type="text" class="form-control" name="symbol" value="<?php echo $this->input->post('symbol') != null ? $this->input->post('symbol') : $details->symbol; ?>" autofocus>
               <?php echo form_error('symbol','<p class="text-danger">','</p>'); ?>
            </div>
            <div class="form-group col-md-12">
               <label for="symbol">Name</label>
               <input type="text" class="form-control" name="name" value="<?php echo $this->input->post('name') != null ? $this->input->post('name') : $details->name; ?>">
               <?php echo form_error('name','<p class="text-danger">','</p>'); ?>
            </div>
            <div class="form-group col-md-12">
               <label for="sp">S&amp;P</label>
               <select class="form-control" name="sp">
                  <option value="0"<?php echo $details->sp ? ' selected' : ''; ?>>No</option>
                  <option value="1"<?php echo $details->sp ? ' selected' : ''; ?>>Yes</option>
               </select>
               <?php echo form_error('sp','<p class="text-danger">','</p>'); ?>
            </div>
            <div class="form-group col-md-12">
               <label for="status">Status</label>
               <select class="form-control" name="status">
                  <option value="Active"<?php echo $details->status == 'Active' ? ' selected' : '';?>>Active</option>
                  <option value="Inactive"<?php echo $details->status == 'Inactive' ? ' selected' : '';?>>Inactive</option>
               </select>
               <?php echo form_error('status','<p class="text-danger">','</p>'); ?>
            </div>
            <div class="col-md-12 bottom-offset-20">
               <button class="btn btn-success btn-block" type="submit">Update</button>
               <a href="<?php echo base_url(); ?>" class="btn btn-default btn-block"><i class="fa fa-chevron-left"></i> back</a>
            </div>
         <?php echo form_close(); ?>
      </div>
   </div>
</section>
