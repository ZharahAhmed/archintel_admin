<section class="row">
   <div class="col-md-10 col-md-offset-1 bordered bottom-offset-20">
      <div class="smpl-step" style="border-bottom: 0; min-width: 500px;">
         <div class="col-xs-4 smpl-step-step active">
            <div class="text-center smpl-step-num">Step 1</div>
            <div class="progress">
               <div class="progress-bar"></div>
            </div>
            <a class="smpl-step-icon"><i class="fa fa-user active" style="font-size: 60px; padding-left: 12px; padding-top: 3px;"></i></a>
            <div class="smpl-step-info text-center">Account Information</div>
         </div>
         <div class="col-xs-4 smpl-step-step disabled">
            <div class="text-center smpl-step-num">Step 2</div>
               <div class="progress">
                  <div class="progress-bar"></div>
               </div>
            <a class="smpl-step-icon"><i class="fa fa-dollar" style="font-size: 60px; padding-left: 17px; padding-top: 7px; "></i></a>
            <div class="smpl-step-info text-center">Subscription Option</div>
         </div>
         <div class="col-xs-4 smpl-step-step disabled">
            <div class="text-center smpl-step-num">Step 3</div>
               <div class="progress">
                  <div class="progress-bar"></div>
               </div>
            <a class="smpl-step-icon"><i class="fa fa-check" style="font-size: 60px; padding-left: 6px; padding-top: 6px;"></i></a>
            <div class="smpl-step-info text-center">Registration Complete</div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-8 col-md-offset-2" style="margin-top: 20px;" >
            <?php echo form_open(''); ?>
               <div class="col-md-12 form-group">
                  <div class="col-md-6">
                     <label for="title">Title</label>
                     <input type="text" class="form-control" name="title" autofocus>
                  </div>
               </div>
               <div class="col-md-12 form-group">
                  <div class="col-md-6">
                     <label for="firstname">Firstname</label>
                     <input type="text" class="form-control" name="firstname">
                  </div>
                  <div class="col-md-6">
                     <label for="lastname">Lastname</label>
                     <input type="text" class="form-control" name="lastname">
                  </div>
               </div>
               <div class="col-md-12 form-group">
                  <div class="col-md-6">
                     <label for="email">Email</label>
                     <input type="text" class="form-control" name="email" placeholder="email@company.com">
                  </div>
                  <div class="col-md-6">
                     <label for="phone">Phone</label>
                     <input type="text" class="form-control" name="phone" placeholder="(111)111-1111">
                  </div>
               </div>
               <div class="col-md-12 form-group">
                  <div class="col-md-6">
                     <label for="company">Company</label>
                     <input type="text" class="form-control" id="autocomplete" name="company">
                  </div>
               </div>
            <?php echo form_close(); ?>
         </div>
      </div>
   </div>
</section>
