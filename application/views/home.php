<div class="row">
	<section class="col-md-12">
	<div class="col-md-6">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title text-center">WALBRO MANAGER</h3>
			</div>
			<div class="panel-body">
				<table class="table table-responsive table-hover">
					<thead>
						<tr>
							<th class="text-center">Symbol</th>
							<th>Name</th>
							<th class="text-center">S&amp;P</th>
							<th class="text-center">Status</th>
							<th class="text-center">Actions</th>
						</tr>
					</thead>
					<tbody>
					<?php foreach($walbro_companies->result() as $walbro): ?>
						<tr>
							<td class="text-center"><?php echo $walbro->symbol; ?></td>
							<td><?php echo $walbro->name; ?></td>
							<td class="text-center"><?php echo $walbro->sp ? 'Yes' : 'No'; ?></td>
							<td class="text-center"><?php echo $walbro->status == 'Active' ? 'Active' : 'Inactive'; ?></td>
							<td class="text-center"><a href="<?php echo base_url('walbro/edit/'.$walbro->ID); ?>">Edit</a> | <a href="<?php echo base_url('walbro/delete/'.$walbro->ID); ?>">Delete</a></td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
			</div>
			<div class="panel-footer">
				<small>Companies with asterisk (*) are included in the S&amp;P list<span class="pull-right"><a href="<?php echo base_url('walbro/add'); ?>">Add Company</a></span></small>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title text-center">DOWNLOAD MANAGER</h3>
			</div>
			<div class="panel-body">
				<ul id="download-manager-list">
					<li>
						<a class="ajax-call" href="<?php echo base_url('ajax/download-media'); ?>">Download Media</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<?php echo form_open(''); ?>
			<fieldset>
				<div class="form-group">
					<div class="row">
						<label for="start_date">Range:</label>
					</div>
					<div class="row bottom-offset-10">
						<div class="col-md-6">
							<input type="text" name="start" class="form-control">
						</div>
						<div class="col-md-6">
							<input type="text" name="end" class="form-control">
						</div>
					</div>
					<div class="row">
						<a class="ajax-call btn btn-info btn-block" href="<?php echo base_url('ajax/download-media'); ?>">Update Historical Data</a>
					</div>
				</div>
			</fieldset>
		<?php echo form_close(); ?>
	</div>
	<div class="col-md-3">

	</div>
	<div class="col-md-3">

	</div>
</section>
</div>