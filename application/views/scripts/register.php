<script type="text/javascript">
   var countries = [
       { value: 'Andorra', data: 'AD' },
       // ...
       { value: 'Zimbabwe', data: 'ZZ' }
   ];

   $('#autocomplete').autocomplete({
       lookup: countries,
       onSelect: function (suggestion) {
           if(indexOf2(countries, 'value', suggestion.data) == -1){
             alert('Selection is not in the list');
          }else{
             alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
          }
       }
   }).blur(function(){
      console.log(this.value);
   })

   function indexOf2(array, data, value){
      for(var i = 0; i < array.length; i++) {
         if(array[i].data === value) {
            return i;
         }
      }

      return -1;
   }
</script>
