<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

   function __construct(){
      parent::__construct();

      $this->load->model('Api_model');
      $this->load->library('Salesforce');

      if($this->uri->segment(2) != 'message'){
         if($this->input->get('api_key') == null){
            $this->session->ApiMessage('API key is missing');

            redirect(base_url('api/message'), 'refresh');
         }else if(!$this->Api_model->ValidAPIKey($this->input->get('api_key'))){
            $this->session->ApiMessage('API key is invalid');

            redirect(base_url('api/message'), 'refresh');
         }
      }
   }

   public function download_account($id){
      $account = $this->salesforce->DownloadAccountDetails($id);

      if(empty($account)){
         $data['json_message'] = array('status' => 'STATUS_FAILED', 'message' => 'Invalid account information');
      }else{
         if($this->Archintel_model->UpdateAccount($account, $id)){
            $data['json_message'] = array('status' => 'STATUS_OK', 'message' => 'Account successfully updated');
         }else{
            $data['json_message'] = array('status' => 'STATUS_FAILED', 'message' => 'Account download failed');
         }
      }

      $this->load->view('json_message', $data);
   }

   public function download_media(){
      $media = $this->salesforce->DownloadMedia();

      if(empty($media)){
         $data['json_message'] = array('status' => 'NO_DATA', 'message' => 'No media to download');
      }else{
         if($this->Archintel_model->UploadMedia($media)){
            $data['json_message'] = array('status' => 'STATUS_OK', 'message' => 'Media successfully updated');
         }else{
            $data['json_message'] = array('status' => 'STATUS_FAILED', 'message' => 'Media download failed');
         }
      }

      $this->load->view('json_message', $data);
   }

   public function download_salesforce_account(){
      if($this->input->get('id') == null){
         $this->session->ApiMessage('Salesforce ID is not specified');
      }else{
         $account = $this->salesforce->DownloadSalesforceAccount($this->input->get('id'));
         $contacts = $this->salesforce->GetContactsByAccountID($this->input->get('id'));

         if($this->Archintel_model->UploadAccountFromSalesforce($account, $contacts)){
            $data['json_message'] = array('status' => 'STATUS_OK', 'message' => 'Account with contacts successfully downloaded');
         }else{
            $data['json_message'] = array('status' => 'STATUS_FAILED', 'message' => 'Downloading of account with contacts failed');
         }

         $this->load->view('json_message', $data);
      }
   }

	public function index()
	{
		return;
	}

   public function message(){
      $data['json_message'] = $this->session->ApiMessage()[0];
      $this->load->view('json_message', $data);
   }
}
