<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Walbro extends CI_Controller {

   function __construct(){
      parent::__construct();

      if(!$this->session->ValidSession()){
         $this->session->ToastErrorMessage('Sign in required');
         redirect(base_url('auth/signin'), 'refresh');
      }
   }

   function _checksymbol($symbol){
      $details = $this->Archintel_model->CompanyDetails($symbol, 'symbol');

      if($details == null){
         return true;
      }

      if($details->ID != $this->input->post('id')){
         $this->form_validation->set_message('_checksymbol','The symbol already exists');
         return false;
      }

      return true;
   }

   public function add(){
      $this->load->helper('form');
      $this->load->library('form_validation');

      $this->form_validation->set_rules('symbol','Symbol','required|callback__checksymbol');
      $this->form_validation->set_rules('name','Name','required');

      if($this->form_validation->run() === false){
         $data['content'] = 'walbro/add';
         $this->load->view('tpl/full-page/main', $data);
      }else{
         $data = array(
            'symbol'       => $this->input->post('symbol'),
            'name'         => $this->input->post('name'),
            'sp'           => $this->input->post('sp'),
            'status'       => $this->input->post('status')
         );

         if($this->Archintel_model->AddCompany($data)){
            $this->session->ToastSuccessMessage("A new record has been successfully added");
         }else{
            $this->session->ToastErrorMessage("The adding of new record has failed");
         }

         redirect(base_url(), 'refresh');
      }
   }

   public function edit($id){
      $this->load->helper('form');
      $this->load->library('form_validation');

      $this->form_validation->set_rules('symbol','Symbol','required|callback__checksymbol');
      $this->form_validation->set_rules('name','Name','required');

      if($this->form_validation->run() === false){
         $data['details'] = $this->Archintel_model->CompanyDetails($id);
         $data['content'] = 'walbro/edit';
         $this->load->view('tpl/full-page/main', $data);
      }else{
         $data = array(
            'symbol'    => $this->input->post('symbol'),
            'name'      => $this->input->post('name'),
            'sp'        => $this->input->post('sp'),
            'status'    => $this->input->post('status')
         );

         $this->Archintel_model->UpdateCompany($data, $this->input->post('id'));
         $this->session->ToastSuccessMessage('The selected record has been successfully updated');
         redirect(base_url(), 'refresh');
      }

   }

   public function delete($id){

      $this->load->helper('form');

      if($this->input->post('delete') == null){
         $data['details'] = $this->Archintel_model->CompanyDetails($id);
         $data['content'] = 'walbro/delete';
         $this->load->view('tpl/full-page/main', $data);
      }else{
         if($this->Archintel_model->DeleteCompany($id)){
            $this->session->ToastSuccessMessage('The selected record has been successfully deleted');
         }else{
            $this->session->ToastErrorMessage('An error occured while trying to delete the selected record');
         }

         redirect(base_url(), 'refresh');
      }
   }

	public function index()
	{
		//$data['content'] = 'home';
		//$this->load->view('tpl/full-page/main', $data);
      redirect(base_url());
	}

}
