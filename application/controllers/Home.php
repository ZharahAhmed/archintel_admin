<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if(!$this->Auth_model->SetupDone()){
			$this->session->ToastErrorMessage('Admin setup is required');
			redirect(base_url('auth/setup'), 'refresh');
		}

		if(!$this->session->ValidSession()){
			$this->session->ToastErrorMessage('Sign in required');
			redirect(base_url('auth/signin'), 'refresh');
		}
	}

	public function index()
	{
		$this->load->helper('form');

		$data['content'] = 'home';
		$data['js'] = array();
		$data['walbro_companies'] = $this->Archintel_model->GetCompanies();
		/*
		base_url('assets/js/third-party/amcharts/amcharts.js'),
		base_url('assets/js/third-party/amcharts/serial.js'),
		base_url('assets/js/third-party/amcharts/themes/patterns.js'),
		base_url('assets/js/third-party/amcharts/plugins/export/export.js'),
		base_url('assets/js/html2canvas.js')
		*/

		/*
		base_url('assets/js/third-party/amcharts/plugins/export/libs/pdfmake/pdfmake.min.js'),
		base_url('assets/js/third-party/amcharts/plugins/export/libs/jszip/jszip.min.js'),
		base_url('assets/js/third-party/amcharts/plugins/export/libs/fabric.js/fabric.min.js'),
		base_url('assets/js/third-party/amcharts/plugins/export/libs/FileSaver.js/FileSaver.min.js'),
		base_url('assets/js/third-party/amcharts/plugins/export/libs/xlsx/xlsx.min.js'),
		base_url('assets/js/third-party/amcharts/plugins/export/libs/blob.js/blob.js')
		*/
		$data['css'] = array();
		/*
		base_url('assets/js/third-party/amcharts/plugins/export/export.css')
		*/
		$data['scripts'] = array();
		$this->load->view('tpl/full-page/main', $data);
	}

	public function test(){
		$this->archintel->UploadHistoricalData($this->archintel->AllCompanyHistoricalData('2016/02/29','2016/03/18'));
		//echo $this->db->last_query();
	}
}
