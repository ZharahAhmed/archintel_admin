<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends CI_Controller {

   function __construct(){
      parent::__construct();

      $this->load->model('Auth_model');
      $this->load->model('Account_model');
   }
   
     public function Accounts(){
    
         
         $this->load->helper('form');
         $this->load->library('form_validation');


         $data['cust'] = $this->Archintel_model->getalluser();
         $data['content'] = 'Account/User_Account';
         $this->load->view('tpl/full-page/main',$data);


   }
   
  public function Edit($id){
         $this->load->helper('form');
         $this->load->library('form_validation');
         $this->load->library('session');

        
       $data['edit'] = $this->Account_model->view($id);


         $data['content'] = 'Account/Edit';
         $this->load->view('tpl/full-page/main', $data);
   }


 public function edited(){
     
        $this->load->library('bcrypt');

         $id= $this->input->post('id');
         $data = array(
         'username' => $this->input->post('username'),
         'email' => $this->input->post('email'),
         
         /*'password' => $this->bcrypt->hash($this->input->post('password')),*/
         );
         $this->Account_model->update($id,$data);
        redirect(base_url(), 'refresh');
       



   }

  public function delete($id){
         $this->load->helper('form');
         $this->load->library('form_validation');
         $this->load->library('session');

        
       $data['edit'] = $this->Account_model->view($id);


         $data['content'] = 'Account/Delete_Account';
         $this->load->view('tpl/full-page/main', $data);

   }

 public function deleted(){
     
        $this->load->library('bcrypt');

         $id= $this->input->post('id');
         $data = array(
         'username' => $this->input->post('username'),
         'email' => $this->input->post('email'),
         
         /*'password' => $this->bcrypt->hash($this->input->post('password')),*/
         );
         $this->Account_model->delete($id,$data);
       
   }
 public function adduser()
    {

  $this->load->helper('form');
         $this->load->library('form_validation');
          $this->load->model('Account_model');
        
        $this->form_validation->set_rules('username', 'Username', 'required|max_length[30]');
        $this->form_validation->set_rules('email', 'Email', 'required|max_length[30]');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[30]');
        

        if ($this->form_validation->run() === TRUE)
        {
            $this->Account_model->adduser();
           
              /* $this->session->set_userdata($Account_model); */ 
         
        }

       $data['content'] = 'Account/adduser';
         $this->load->view('tpl/full-page/main', $data);
    }




}