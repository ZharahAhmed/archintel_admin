<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

   function __construct(){
      parent::__construct();

      $this->load->model('Auth_model');
   }

   function _validateuser($username){
      $details = $this->Auth_model->AllDataDetails('users','username', $username);

      if(strlen($this->input->post('password')) == 0){
         $this->form_validation->set_message('_validateuser','Password field cannot be empty');
         return false;
      }

      $this->form_validation->set_message('_validateuser','Invalid username or password');

      if($details->num_rows() > 0){
         $this->load->library('Bcrypt');

         if($this->bcrypt->verify($this->input->post('password'), $details->row()->password)){
            return true;
         }
      }
      return false;
   }

	public function index()
	{

	}

   public function register(){
      $this->load->helper('form');
      $this->load->library('form_validation');

      $data['content'] = 'auth/register';
      $data['js'] = array(base_url('assets/js/jquery.autocomplete.min.js'));
      $data['scripts'] = array('scripts/register');
      $this->load->view('tpl/full-page/main', $data);
   }

   public function setup(){
      if($this->Auth_model->SetupDone()){
         redirect(base_url());
      }

      $this->load->helper('form');
      $this->load->library('form_validation');

      $this->form_validation->set_rules('username','Username','required|min_length[4]');
      $this->form_validation->set_rules('email','E-mail','required|valid_email|is_unique[users.email]');
      $this->form_validation->set_rules('password','Password','required|min_length[6]');
      $this->form_validation->set_rules('vpassword','Verify password','required|matches[password]');
      $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable col-md-10 col-md-offset-1"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>','</div>');

      if($this->form_validation->run() === false){
         $data['content'] = 'auth/setup';
         $this->load->view('tpl/full-page/main', $data);
      }else{
         $this->load->library('Bcrypt');

         $data = array(
            'username'        => $this->input->post('username'),
            'email'           => $this->input->post('email'),
            'password'        => $this->bcrypt->hash($this->input->post('password')),
            'user_type'       => 'Admin',
            'status'          => 'Active'
         );

         if($this->Auth_model->Setup($data)){
            $this->session->ToastSuccessMessage('Setup done, you may now sign in');
            redirect(base_url('auth/signin'), 'refresh');
         }else{
            $this->session->ToastErrorMessage('Setup failed');
            redirect(base_url('auth/setup'), 'refresh');
         }
      }
   }

   public function signin(){
      if($this->session->ValidSession()){
         redirect(base_url());
      }

      $this->load->helper('form');
      $this->load->library('form_validation');

      $this->form_validation->set_rules('username','Username','required|callback__validateuser');
      $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable col-md-10 col-md-offset-1"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4>Error!</h4>','</div>');

      if($this->form_validation->run() === false){
         $data['content'] = 'auth/signin';
         $this->load->view('tpl/full-page/main', $data);
      }else{
         $data = array(
            'last_activity' => date('Y-m-d H:i:s', time()),
            'username' => $this->input->post('username'),
            'ip_address' => $this->input->ip_address()
         );

         $this->session->SetSession($data);

         if($this->input->post('next') != null){
            redirect($this->input->post('next'));
         }

         redirect(base_url(),'refresh');
      }
   }

   public function signout(){
      if($this->session->ValidSession()){
         $this->session->Destroy();
         $this->session->ToastSuccessMessage('You have been signed out');
         redirect(base_url('auth/signin'), 'refresh');
      }else{
         redirect(base_url());
      }
   }

   

}

